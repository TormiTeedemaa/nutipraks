import RPi.GPIO as GPIO 
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)

GPIO.setup(7, GPIO.OUT)  #led nr 1
GPIO.setup(11, GPIO.OUT) #led nr 2
while True:
    GPIO.output(7,GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(11,GPIO.HIGH)
    GPIO.output(7,GPIO.LOW)
    time.sleep(0.5)
    GPIO.output(11,GPIO.LOW)
GPIO.cleanup()