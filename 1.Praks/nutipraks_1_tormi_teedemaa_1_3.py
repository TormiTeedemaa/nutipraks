import RPi.GPIO as GPIO 
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)

GPIO.setup(11, GPIO.OUT)  #led nr 10 ei toota
GPIO.setup(40, GPIO.OUT)  #led nr 9
GPIO.setup(35, GPIO.OUT)  #LED NR 7
GPIO.setup(38, GPIO.OUT)  #led nr 8
GPIO.setup(33, GPIO.OUT)  #led nr 5
GPIO.setup(36, GPIO.OUT)  #led nr 6
GPIO.setup(31, GPIO.OUT)  #led nr 3
GPIO.setup(29, GPIO.OUT)  #led nr 2
GPIO.setup(32, GPIO.OUT)  #led nr 4
GPIO.setup(7, GPIO.OUT)   #led nr 1

while True:
    GPIO.output(7,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(7,GPIO.LOW)
    
    GPIO.output(29,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(29,GPIO.LOW)
   
    
    GPIO.output(31,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(31,GPIO.LOW)
    
    
    GPIO.output(32,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(32,GPIO.LOW)
    
    
    GPIO.output(33,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(33,GPIO.LOW)
    
    
    GPIO.output(36,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(36,GPIO.LOW)
    
    
    GPIO.output(35,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(35,GPIO.LOW)
  
    
    GPIO.output(38,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(38,GPIO.LOW)
    
    
    GPIO.output(40,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(40,GPIO.LOW)
    
    #reverse
    GPIO.output(38,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(38,GPIO.LOW)
    
    GPIO.output(35,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(35,GPIO.LOW)
    
    GPIO.output(36,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(36,GPIO.LOW)
    
    GPIO.output(33,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(33,GPIO.LOW)
    
    GPIO.output(32,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(32,GPIO.LOW)
    
    GPIO.output(31,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(31,GPIO.LOW)
    
    GPIO.output(29,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(29,GPIO.LOW)
    
    
GPIO.cleanup()