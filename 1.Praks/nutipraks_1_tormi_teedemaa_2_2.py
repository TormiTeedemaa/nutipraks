import RPi.GPIO as GPIO
import time
import random  
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
#viigud
viik = []
#viikude seadmine
GPIO.setup(40, GPIO.OUT)#E
GPIO.setup(38, GPIO.OUT)#D
GPIO.setup(36, GPIO.OUT)#T2pike
GPIO.setup(32, GPIO.OUT)#C

GPIO.setup(37, GPIO.OUT)#G
GPIO.setup(35, GPIO.OUT)#F
GPIO.setup(33, GPIO.OUT)#A
GPIO.setup(31, GPIO.OUT)#B
#nupp
GPIO.setup(7,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)

koik =[31,33,35,37,32,36,38,40]
null = [31,33,35,32,38,40]
uks = [31,32]
kaks = [31,33,37,38,40]
kolm = [31,32,33,37,38]
neli = [31,32,35,37]
viis = [33,37,35,32,38]
kuus = [33,35,37,32,38,40]
seitse = [32,31,33]
kaheksa = [31,33,35,37,32,38,40]
uheksa = [31,33,35,37,32,38]

numbrid = [null,uks,kaks,kolm,neli,viis,kuus,seitse,kaheksa,uheksa]
#tuled kustu
GPIO.output(koik, GPIO.HIGH)
button_press = 0
i = 0

def tavaline():
    global i
    global button_press
    while button_press == 0:
        GPIO.output(numbrid[i],GPIO.LOW)
        time.sleep(1)
        GPIO.output(numbrid[i],GPIO.HIGH)
        i += 1
        if i == 10:
            i = 0
        

def tagurpidi():
    global button_press
    global i
    i-=1
    while button_press == 1:
        GPIO.output(numbrid[i],GPIO.LOW)
        time.sleep(1)
        GPIO.output(numbrid[i],GPIO.HIGH)
        i -= 1
        if i == -1:
            i = 9
    
    
def nupp(channel):
    global button_press
    print('midagi')
    button_press +=1
    if button_press == 3:
        button_press = 0

def suvaline():
    global button_press
    global numbrid
    while button_press == 2:
        valik = random.choices(numbrid)
        GPIO.output(valik[0],GPIO.LOW)
        time.sleep(1)
        GPIO.output(valik[0],GPIO.HIGH)

GPIO.add_event_detect(7, GPIO.RISING, callback = nupp,bouncetime=300)   
while True:
    tavaline()
    tagurpidi()
    suvaline()
    
    
