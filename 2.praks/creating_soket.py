# partial socket client example in python
# file: juhendid-sockets-nutipraks-2020-code-client-http-04-server_port.py
# adapted by: Jyri J6ul

# initialise constants

port = 80
import socket  # for sockets

# create an AF_INET, STREAM socket (TCP)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connect to remote server
s.connect((remote_ip, port))

print('...socket connected to ' + host + ' on IP ' + remote_ip)
