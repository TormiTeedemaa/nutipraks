import RPi.GPIO as GPIO 
import random
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)
sagedus = 0.5
GPIO.setup(11, GPIO.OUT)   #led nr 1
GPIO.setup(7, GPIO.OUT) #led nr 2
while True:
    GPIO.output(11,GPIO.HIGH)
    time.sleep(sagedus)
    GPIO.output(11,GPIO.LOW)
            
    GPIO.output(7,GPIO.HIGH)
    time.sleep(sagedus)
    GPIO.output(7,GPIO.LOW)
