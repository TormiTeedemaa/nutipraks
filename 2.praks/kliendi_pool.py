#!/usr/bin/python3

import socket
import sys, getopt

def usage():
    print("usage: ",sys.argv[0],"-p|--port -H|--host")
    return

def menu():
    try:
        opts, args = getopt.getopt(sys.argv[1:],"p:H:m:h",["port=","host=","help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(sys.argv)==1:
        usage()
        sys.exit(2)

    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit(2)
        elif opt in ("-p","--port"):
            port=int(arg)
        elif opt in ("-H","--host"):
            host=arg

    return port,host

def create_socket(port,host):

    try:
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    except(socket.error):
        print("...failed to create socket...error code: ",str(msg[0])," error message: ",msg[1])
        sys.exit()
    print("...socket created")

    try:
        remote_ip=socket.gethostbyname(host)
    except socket.gaierror:
        print("...hostname could not be resolved...exiting")
        sys.exit()
    print("...IP address of",host," is ",remote_ip)

    s.connect((remote_ip,port))

    print("...socket connected to ",host," on IP ",remote_ip)

    return s


def main():
    [port,host]=menu()
    s=create_socket(port,host)
    
    while True:
        message = str(input('sonum on(ledon,sagedus): '))
        if message == '':
           message = ' '     
        try:
            s.sendall(str.encode(message))
        except socket.error:
            print("...send failed...")
            sys.exit()

        print("...message sent successfully...")

        reply=s.recv(4096)

        print(bytes.decode(reply))
        
        
    
            
            
    s.close()

    return

if __name__=="__main__":
    main()