import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)
import time
#viikude seadmine
GPIO.setup(40, GPIO.OUT)#D
GPIO.setup(38, GPIO.OUT)#E
GPIO.setup(36, GPIO.OUT)#T2PIKE
GPIO.setup(29, GPIO.OUT)#C

GPIO.setup(37, GPIO.OUT)#G
GPIO.setup(35, GPIO.OUT)#F
GPIO.setup(33, GPIO.OUT)#A
GPIO.setup(31, GPIO.OUT)#B

koik =[31,33,35,37,29,36,38,40]
null = [31,33,35,29,38,40,36]
uks = [31,29,36]
kaks = [31,33,37,38,40,36]
kolm = [31,29,33,37,40,36]
neli = [31,29,35,37,36]
viis = [33,37,35,29,40,36]
kuus = [33,35,37,29,38,40,36]
seitse = [29,31,33,36]
kaheksa = [31,33,35,37,29,38,40,36]
uheksa = [31,33,35,37,29,40,36]
numbrid = [null,uks,kaks,kolm,neli,viis,kuus,seitse,kaheksa,uheksa]
GPIO.output(koik, GPIO.HIGH)
while True:
    GPIO.output(null,GPIO.LOW)
    time.sleep(1)
    GPIO.output(null,GPIO.HIGH)
    
    GPIO.output(uks,GPIO.LOW)
    time.sleep(1)
    GPIO.output(uks,GPIO.HIGH)
    
    GPIO.output(kaks,GPIO.LOW)
    time.sleep(1)
    GPIO.output(kaks,GPIO.HIGH)
    
    GPIO.output(kolm,GPIO.LOW)
    time.sleep(1)
    GPIO.output(kolm,GPIO.HIGH)
    
    GPIO.output(neli,GPIO.LOW)
    time.sleep(1)
    GPIO.output(neli,GPIO.HIGH)
   
    GPIO.output(viis,GPIO.LOW)
    time.sleep(1)
    GPIO.output(viis,GPIO.HIGH)
    
    GPIO.output(kuus,GPIO.LOW)
    time.sleep(1)
    GPIO.output(kuus,GPIO.HIGH)
    
    GPIO.output(seitse,GPIO.LOW)
    time.sleep(1)
    GPIO.output(seitse,GPIO.HIGH)
    
    GPIO.output(kaheksa,GPIO.LOW)
    time.sleep(1)
    GPIO.output(kaheksa,GPIO.HIGH)
    
    GPIO.output(uheksa,GPIO.LOW)
    time.sleep(1)
    GPIO.output(uheksa,GPIO.HIGH)

