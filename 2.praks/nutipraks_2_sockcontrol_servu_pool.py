#!/usr/bin/python3

import RPi.GPIO as GPIO 
import socket 
import sys, getopt, time
from _thread import *
from subprocess import call
import random

sagedus = 0.5
vilgutamine = 0
polema = 0
i = 0 #kliendi counter
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)

#2 led vilkumise setup
GPIO.setup(11, GPIO.OUT) #led nr 1
GPIO.setup(7, GPIO.OUT)  #led nr 2

#viikude seadmine numbri ledile
GPIO.setup(40, GPIO.OUT)#D
GPIO.setup(38, GPIO.OUT)#E
GPIO.setup(36, GPIO.OUT)#T2PIKE
GPIO.setup(29, GPIO.OUT)#C

GPIO.setup(37, GPIO.OUT)#G
GPIO.setup(35, GPIO.OUT)#F
GPIO.setup(33, GPIO.OUT)#A
GPIO.setup(31, GPIO.OUT)#B

koik =[31,33,35,37,29,36,38,40]
null = [31,33,35,29,38,40,36]
uks = [31,29,36]
kaks = [31,33,37,38,40,36]
kolm = [31,29,33,37,40,36]
neli = [31,29,35,37,36]
viis = [33,37,35,29,40,36]
kuus = [33,35,37,29,38,40,36]
seitse = [29,31,33,36]
kaheksa = [31,33,35,37,29,38,40,36]
uheksa = [31,33,35,37,29,40,36]
numbrid = [null,uks,kaks,kolm,neli,viis,kuus,seitse,kaheksa,uheksa]
GPIO.output(koik, GPIO.HIGH)

HOST='' # symbolic name meaning all available interfaces
 # arbitrary non-privileged port
def vilkumine():
    global vilgutamine,sagedus
    while True:
        if vilgutamine == 1:
            GPIO.output(11,GPIO.HIGH)
            time.sleep(sagedus)
            GPIO.output(11,GPIO.LOW)
            
            GPIO.output(7,GPIO.HIGH)
            time.sleep(sagedus)
            GPIO.output(7,GPIO.LOW)
        elif vilgutamine == 0:
            GPIO.output(11,GPIO.LOW)
            GPIO.output(7,GPIO.LOW)
            
start_new_thread(vilkumine,())
def numberled():
    global numbrid,koik,null,uks,kaks,kolm,neli,viis,kuus,seitse,kaheksa,uheksa,numbri_count,polema
    numbri_count = 0
    
    while True:
        if polema == 1:
            GPIO.output(numbrid[numbri_count],GPIO.LOW)
            time.sleep(1)
            GPIO.output(numbrid[numbri_count],GPIO.HIGH)
            numbri_count += 1
            if numbri_count == 10:
                numbri_count = 0
        #random number generator
        elif polema == 2:
            valik = random.choices(numbrid)
            GPIO.output(valik[0],GPIO.LOW)
            time.sleep(1)
            GPIO.output(valik[0],GPIO.HIGH)
            
        #tagurpidi
        elif polema == 3:
            GPIO.output(numbrid[numbri_count],GPIO.LOW)
            time.sleep(1)
            GPIO.output(numbrid[numbri_count],GPIO.HIGH)
            numbri_count -= 1
            if numbri_count == -1:
                numbri_count = 9
        elif polema == 0:
            GPIO.output(koik,GPIO.HIGH)
start_new_thread(numberled,())       
def usage():
    print("usage: ",sys.argv[0]," -p|--port PORT")
    return

def menu():
    try:
        opts,args = getopt.getopt(sys.argv[1:],"p:h",["port=","help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if len(sys.argv)==1:
        usage()
        sys.exit(2)

    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit()
        elif opt in ("-p","--port"):
            PORT=int(arg)

    return PORT

def create_socket(HOST,PORT):
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    print("socket created")

# bind socket to local host and port

    try:
        s.bind((HOST,PORT))
    except(socket.error, msg):
        print("...bind failed...error code: ",str(msg[0]),", error message: ",msg[1])
        sys.exit()

    print("...socket bind complete...")

    # start listening on socket

    s.listen(10)
    print("...socket now listening...")

    return s

# function for handling connections...this will be used to create threads
def clientthread(conn):
    global vilgutamine,sagedus,polema,i
    # sending message to connected client
#conn.send('...welcome to the server...type something and hit enter \n') # send only takes strings
    # infinite loop so that function do not terminate and thread do not end
    id = i
    while True:
        # receiving from client
        data=conn.recv(1024)
        reply="...OK..."+str(id) + bytes.decode(data)
        data1=bytes.decode(data)
        if not data:
            break
        print(time.asctime( time.localtime(time.time()) ),":",data1)

        conn.sendall(str.encode(reply))
        if id == 1:
            if data1.find('ledon')==0:
                sagedus = float(data1.split(',')[1])
                print("turn led on")
                vilgutamine = 1
            
            elif data1.find('ledoff')==0:
                print("turn led off")
                vilgutamine = 0
        elif id == 2:
            if data1.find('numbrid on')==0:
                polema = 1
                print("numbreid hakatakse kuvama")
            
            elif data1.find('numbrid suvalised')==0:
                polema = 2
                print("numbreid hakatakse kuvama suvaliselt")
            
            elif data1.find('numbrid tagurpidi')==0:
                polema = 3
                print("numbreid haktakse kuvama tagurpidi jarjekorras")
            elif data1.find('numbrid kustu')==0:
                polema = 0
                print("numbrid pannakse kustu")
            

    # came out of loop
    conn.close()

# now keep talking with the client

def main():
    global i
    PORT=menu()
    s=create_socket(HOST,PORT)
    try:
        while 1:
    # wait to accept a connection - blocking call
            conn, addr = s.accept()

    # display client information
            print("...connected with ",addr[0],":",str(addr[1]))

    # start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function
            start_new_thread(clientthread,(conn,))
            i += 1
    except KeyboardInterrupt:
        print('sockett kinnnni')
        s.close()

    return

if __name__=="__main__":
    main()
