import RPi.GPIO as GPIO 
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)
GPIO.setup(7, GPIO.OUT)   #led nr 1
GPIO.setup(40, GPIO.OUT) #led nr 2
sagedus = 0.5
while True:
    GPIO.output(40,GPIO.HIGH)
    time.sleep(sagedus)
    GPIO.output(40,GPIO.LOW)
    
    GPIO.output(7,GPIO.HIGH)
    time.sleep(sagedus)
    GPIO.output(7,GPIO.LOW)
    
    
    

GPIO.cleanup()
