import RPi.GPIO as GPIO 
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)
GPIO.setup(38, GPIO.OUT)   #led nr 1
GPIO.setup(40, GPIO.OUT) #led nr 2

GPIO.output(40,GPIO.LOW)
GPIO.output(38,GPIO.LOW)

GPIO.cleanup()