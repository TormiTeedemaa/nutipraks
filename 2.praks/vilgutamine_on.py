import RPi.GPIO as GPIO 
import time
GPIO.setmode(GPIO.BOARD) #seadistab GPIO esilekutsumiseks pin meetodi
GPIO.setwarnings(False)
GPIO.setup(38, GPIO.OUT)   #led nr 1
GPIO.setup(40, GPIO.OUT) #led nr 2
sagedus = 0.5
vilgutamine = 1
while True:
    if vilgutamine > 0 and vilgutamine < 4:
        GPIO.output(40,GPIO.HIGH)
        time.sleep(sagedus)
        GPIO.output(40,GPIO.LOW)
            
        GPIO.output(38,GPIO.HIGH)
        time.sleep(sagedus)
        GPIO.output(38,GPIO.LOW)
        vilgutamine += 1 
        #start_new_thread(vilgutamine,(conn,))
    elif vilgutamine == 4:
        GPIO.output(40,GPIO.LOW)
        GPIO.output(38,GPIO.LOW)

        GPIO.cleanup()
    
      
GPIO.cleanup()
