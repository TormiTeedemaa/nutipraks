int led = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(led, OUTPUT);
}

void loop() {
  //1 time unit = 400microseconds
  //3 time unit = 1200 microseconds
  //7 time unit = 2800 microseconds
  //S täht
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(1200);

  //O täht
  digitalWrite(led, HIGH);
  delay(1200);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(1200);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(1200);
  digitalWrite(led, LOW);
  delay(1200);
  
  // S täht
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(400);
  digitalWrite(led, HIGH);
  delay(400);
  digitalWrite(led, LOW);
  delay(2800);


  
}
