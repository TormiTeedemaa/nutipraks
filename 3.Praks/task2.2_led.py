#!/usr/bin/env python3
import serial
import time
if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
    ser.flush()
    
    while True:
        cmd = input("Sisestage kask(ledon,ledoff,status): ")
        ser.write(cmd.encode())
        time.sleep(0.1)
        line = ser.readline().decode().rstrip()
        print(line)
        time.sleep(1)