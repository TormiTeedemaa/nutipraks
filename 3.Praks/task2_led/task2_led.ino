int led = 5;
String status = "off";
void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    String data = Serial.readString();
    if (data == "ledon") {
      digitalWrite(led, HIGH);
      status = "on";
      Serial.println("Led on põlema pandud");
    } else if (data == "ledoff") {
      digitalWrite(led, LOW);
      status = "off";
      Serial.println("Led on kustutatud");
    } else if (data == "status") {
      Serial.println(status);

    }
  }
}
