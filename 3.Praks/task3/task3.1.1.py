#!/usr/bin/env python3
import serial
import time
from rpi_ws281x import*
import argparse
import matplotlib.pyplot as plt
import numpy as np
x = []
y = []
i = 0
LED_COUNT = 1      # Number of LED pixels.
LED_PIN = 12     # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0

def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)
def mappper(x, in_min, in_max, out_min,out_max):
    return int((x-in_min)*(out_max-out_min)/(in_max-in_min)+out_min)
f = open("temperatuur.txt", "w+")


if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
    ser.flush()
    
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    strip.begin()

    while True:
        line = ser.readline().decode().rstrip()
        
        print("Temperatuur on: ", (line))
        f.write("Temperatuur on: " + (line) + "\n")
        temp = mappper(float(line), 18, 31, 0, 255)
        colorWipe(strip,Color(temp, 0, int(((255-temp))*0.5)))
        time.sleep(1)
        x.append(i)
        y.append(float(line))
        plt.plot(x,y)
        plt.pause(0.05)
        plt.legend()
        plt.show(block=False)
        i -= -1       
        
f.close()
        

