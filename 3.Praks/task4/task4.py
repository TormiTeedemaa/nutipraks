#!/usr/bin/env python3
import lcd_baas
import time
import serial
mylcd = lcd_baas.lcd()

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
    ser.flush()

    while True:
        potentsiomeeter = ser.readline().decode().rstrip()
        print("Pinge on: ", (potentsiomeeter))
        mylcd.lcd_clear()
        mylcd.lcd_display_string(str(potentsiomeeter), 1)
        mylcd.lcd_display_string("VOLTI", 2)
        
        
