import lcd_baas
import time

mylcd = lcd_baas.lcd()
while True:
    mylcd.lcd_display_string("FAST AND FURIOUS", 1)
    time.sleep(3)
    mylcd.lcd_clear()

    mylcd.lcd_display_string("RIDE or DIE", 1)
    time.sleep(3)
    mylcd.lcd_clear()
