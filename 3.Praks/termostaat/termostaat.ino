#include <OneWire.h>
#include <DallasTemperature.h>

const int SENSOR_PIN = 5;
OneWire oneWire(SENSOR_PIN);
DallasTemperature sensors(&oneWire);
float tempCelsius;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  sensors.begin();
  

}

void loop() {
  // put your main code here, to run repeatedly:
  sensors.requestTemperatures();
  tempCelsius = sensors.getTempCByIndex(0);
  Serial.println(tempCelsius);
  delay(1000);

}
