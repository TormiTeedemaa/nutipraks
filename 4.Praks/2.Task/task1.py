#!/usr/bin/env python3
import serial
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.OUT)
staatus = 'led kinni'
if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
    ser.flush()
    
    while True:
        if ser.in_waiting > 0:
            ser.write(b"COMMANDID:ledon;ledoff;staatus\n")
            line = ser.readline().decode().rstrip()
            print(line + "\n")
            ser.write(b"Message received!\n")
            
            if line == 'ledon':
                staatus = 'led poleb'
                GPIO.output(40, GPIO.HIGH)
            elif line == 'ledoff':
                staatus = 'led kinni'
                GPIO.output(40, GPIO.LOW)
            elif line == "staatus":
                if staatus == 'led poleb':
                    ser.write(b"LED POLEB\n")
                elif staatus == 'led kinni':
                    ser.write(b"LED on KUSTUS\n")
                
                
          