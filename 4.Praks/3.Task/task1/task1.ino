#include <SoftwareSerial.h>
SoftwareSerial bt(2, 3);
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);
String cmd = "";


void setup() {
  //Initialize Serial Monitor
  Serial.begin(9600);
  //Initialize Bluetooth Serial Port
  bt.begin(9600);
  lcd.init();
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(2, 0);
}

void loop() {
  //Read data from HC06
  cmd = "";
  while (bt.available() > 0) {
    cmd += (char)bt.read();
    delay(100);

  }
  if (cmd != "") {
    Serial.print("Command recieved : ");
    Serial.println(cmd);
    lcd.init();
    lcd.clear();
    lcd.backlight();
    lcd.setCursor(2, 0);
    lcd.print(cmd);
    delay(100);

  }

}
