#include <IRremote.h>

const int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results results;
const int redPin = 5;


void setup() {
  irrecv.enableIRIn();
  irrecv.blink13(true);
  pinMode(redPin, OUTPUT);

}

void loop() {
  if (irrecv.decode(&results)) {

    switch (results.value) {
      case 0xFF38C7: //Keypad button "5"
        digitalWrite(redPin, LOW);
    }

    switch (results.value) {
      case 0xFF18E7: //Keypad button "2"
        digitalWrite(redPin, HIGH);
    }

    irrecv.resume();
  }
}
