#include <ESP8266WiFi.h>

char ssid[] = "TormiWifi";           // SSID of your AP
char pass[] = "whyyouhackme";         // password of your AP

IPAddress server(192, 168, 4, 15);  // IP address of the AP
WiFiClient client;

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.println("station_bare_01.ino");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
  
}

void loop() {
  client.connect(server, 80);
  int fototakisti = analogRead(A0);
  if (fototakisti > 150) {
    Serial.println(fototakisti);
    client.println("LIGHTS OFF");
    delay(100);
  }
  else if (fototakisti<= 150) {
    Serial.println(fototakisti);
    client.println("LIGHTS ON");
    delay(100);

    String answer = client.readStringUntil('\r');
    Serial.println("From the AP: " + answer);
    client.flush();
    client.stop();
    
  }
}
