#include <ESP8266WiFi.h>

WiFiServer server(80);
IPAddress IP(192, 168, 4, 15);
IPAddress mask = (255, 255, 255, 0);

byte led = D3;

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  WiFi.softAP("TormiWifi", "whyyouhackme");
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();
  pinMode(led, OUTPUT);
  Serial.println();
  Serial.println("accesspoint_bare_01.ino");
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());
}

void loop() {
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  //fototakisti tulenev info
  String request = client.readStringUntil('\r');
  if (request == "LIGHTS ON") {
    digitalWrite(led, HIGH);
  }
  else {
    digitalWrite(led, LOW);

  }
  Serial.println("********************************");
  Serial.println("From CLIENT CAME INFO: " + request);
  client.flush();
  String saadetud = Serial.readString();
  client.println(saadetud + "\r");
  Serial.println(saadetud);
}
