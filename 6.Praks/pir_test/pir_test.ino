
int pirPin = D4;                 // PIR Out pin
int pirStat = LOW;                   // PIR status
void setup() {
  Serial.begin(115200);
  pinMode(pirPin, INPUT);
  
}
void loop() {
  pirStat = digitalRead(pirPin);
  Serial.println(pirStat);
  if (pirStat == HIGH) {            // if motion detected

    Serial.println("Motion detected");

  }
  else {
    Serial.println("Motion AFK");
  }
  delay(1000);
}
