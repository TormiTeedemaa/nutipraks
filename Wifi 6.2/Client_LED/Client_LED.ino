/*  Accesspoint - station communication without router
 *  see: https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/station-class.rst
 *  Works with: accesspoint_bare_01.ino
 */


#include <SPI.h>
#include <Wire.h>
#include <ESP8266WiFi.h>

char ssid[] = "joeliwifi";           // SSID of your AP
char pass[] = "joeliwifipass";         // password of your AP

String wifiData = "";
const int ledPin = 2;

IPAddress server(192,168,4,10);     // IP address of the AP
WiFiClient client;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.println("station_bare_01.ino");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

void ledOn(){
  digitalWrite(ledPin, HIGH);
  Serial.println("Turning LED on!");
}

void ledOff(){
  digitalWrite(ledPin, LOW);
  Serial.println("Turning LED off!");
}

void loop() {
  client.connect(server, 80);
  client.print("LED\r");
  while(true){
    if(client.available()){
      wifiData = "";
      while(client.available()){
        char ch = client.read();
        wifiData += ch;
        delay(2);
      }
      Serial.println(wifiData);
      if (wifiData == "LEDON"){
        ledOn();
      }
      else if (wifiData == "LEDOFF"){
        ledOff();
      }
    }
    if(!client.connected()){
      break;
    }
  }
  client.stop();
}
