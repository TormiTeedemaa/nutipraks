#include <ESP8266WiFi.h>

const int sensorPin = 0;
char ssid[] = "joeliwifi";           // SSID of your AP
char pass[] = "joeliwifipass";         // password of your AP

IPAddress server(192,168,4,10);     // IP address of the AP
WiFiClient client;

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.println("station_bare_01.ino");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

bool checkSensor(byte pin, int threshold){
  int value = analogRead(pin);
  if(value >= threshold){
    return true;
  }
  else{
    return false;
  }
  
}

void loop() {
  client.connect(server, 80);
  client.print("LGT\r");
  while(true){
    Serial.println("Sensor reading: ");
    Serial.println(analogRead(sensorPin));
    while(!checkSensor(sensorPin, 100)){
      Serial.println("Sensor reading: ");
      Serial.println(analogRead(sensorPin));
      client.print("LGT1");
      client.flush();
    }
    client.print("LGT0");
    client.flush();
  }
  client.stop();
  
}
