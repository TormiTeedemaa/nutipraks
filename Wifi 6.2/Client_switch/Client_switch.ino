/*  Accesspoint - station communication without router
 *  see: https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/station-class.rst
 *  Works with: accesspoint_bare_01.ino
 */

#include <ESP8266WiFi.h>

const int buttonPin = 2; //D4 
bool buttonState = false;
char ssid[] = "joeliwifi";           // SSID of your AP
char pass[] = "joeliwifipass";         // password of your AP

IPAddress server(192,168,4,10);     // IP address of the AP
WiFiClient client;

void setup() {
  pinMode(buttonPin, INPUT);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.println("station_bare_01.ino");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

int checkButton(){
  if(digitalRead(buttonPin)){
    return 0;
  }
  return 1;
}


void loop() {
  client.connect(server, 80);
  client.println("BTN\r");
  while(true){
    int buttonStatus = checkButton();
    client.print("BTN" + String(buttonStatus));
    Serial.println("BTN" + String(buttonStatus) + '\r');
    delay(30);
    client.flush();
  }
  client.stop();
}
