/*  Accesspoint - station communication without router
 *  see: https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/soft-access-point-class.rst
 *       https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/soft-access-point-examples.rst
 *       https://github.com/esp8266/Arduino/issues/504
 *  Works with: station_bare_01.ino
 */ 


#include <ESP8266WiFi.h>

WiFiServer server(80);
IPAddress IP(192,168,4,10);
IPAddress mask = (255, 255, 255, 0);

#define CLIENTCOUNT 3
int con_clients = 0;
WiFiClient clients[CLIENTCOUNT];

String request = "";
String LEDData = "";
String ButtonData = "";
String LightData = "";
int ButtonNumber = 0;
int LEDNumber = 0;
int LightNumber = 0;
bool ledState = false;
unsigned long currentTime = 0;
unsigned long ledTime = 0;
bool manualOn = false;
bool autoOn = false;
const int timeSec = 5; //siin saab muuta aega kauaks tahad et põleks
const long interval = timeSec * 1000; 

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  WiFi.softAP("joeliwifi", "joeliwifipass", 3, false);
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();
  
  Serial.println();
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());
}




void loop() {
  while(con_clients < 3){
    WiFiClient client = server.available();
    if(client){
      Serial.println("New client found!");
      for(int i = 0; i < CLIENTCOUNT; i++){
        if(!clients[i]){
          clients[i] = client;
          Serial.println("Added new client number " + String(i) + "!");
          String message = clients[i].readStringUntil('\r');
          Serial.println(message);
          if(message == "BTN"){
            ButtonNumber = i;
            Serial.println("Button client is number " + String(i));
          }
          else if (message == "LED"){
            LEDNumber = i;
            Serial.println("LED client is number " + String(i));
          }
          else if (message == "LGT"){
            LightNumber = i;
            Serial.println("Light client is number " + String(i));
          }
          con_clients += 1;
          break;
        }
      }
    }
  }
  Serial.println("All clients connected!");
  Serial.println("Client numbers:");
  Serial.println("LED:" + String(LEDNumber));
  Serial.println("Light sensor:" + String(LightNumber));
  Serial.println("Button:" + String(ButtonNumber));

  while(true){
    currentTime = millis();

    if(clients[ButtonNumber].available()){
      ButtonData = "";
      while(clients[ButtonNumber].available()){
        char btnch = clients[ButtonNumber].read();
        ButtonData += btnch;
        delay(2);
      }
      Serial.println(ButtonData);
    }

    if(clients[LightNumber].available()){
      LightData = "";
      while(clients[LightNumber].available()){
        char lgtch = clients[LightNumber].read();
        LightData += lgtch;
        delay(2);
      }
      Serial.println(LightData);
    }
    
    //ButtonData = clients[ButtonNumber].readStringUntil('\r');
    //LightData = clients[LightNumber].readStringUntil('\r');
    
    //delay(50);

    while(true){
      if(ButtonData == "BTN1"){
        if(ledState == false){
          manualOn = true;
          ledState = true;
          break;
        }
        ledState = false;
        manualOn = false;
        break;
      }

      if(LightData == "LGT1" and !manualOn){
        ledState = true;
        ledTime = millis();
        autoOn = true;
        break;
        }
        
      if(autoOn){
        if(currentTime - ledTime >= interval){
          ledState = false;
          autoOn = false;
          break;
        }
      }
      break;
      }

    
    if(ledState == true){
      clients[LEDNumber].print("LEDON");
      clients[LEDNumber].flush(); 
      //Serial.println("LED should turn on");
    }
    else if (ledState == false){
      clients[LEDNumber].print("LEDOFF");
      clients[LEDNumber].flush();
      //Serial.println("LED should turn off");
    }
    
  }
  
 }
