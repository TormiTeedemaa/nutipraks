import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.OUT)
print("led on")
GPIO.output(40, GPIO.HIGH)
time.sleep(3)
print("led off")
GPIO.output(40, GPIO.LOW)