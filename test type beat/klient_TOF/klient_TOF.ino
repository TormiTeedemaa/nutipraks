#include <ESP8266WiFi.h>
#include "Adafruit_VL53L0X.h"
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
char ssid[] = "TormiWifi";           // SSID of your AP
char pass[] = "whyareyouhere"; // password of your AP

int distance_mm = 0;
IPAddress server(192, 168, 4, 15); // IP address of the AP
WiFiClient client;

void setup() {
  Serial.begin(115200);
  lox.begin();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(200);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

void loop() {
  //client.connect(server, 88);
  if (client.connect(server, 8888)){
    Serial.println("connected ", client.localIP()); 
    Serial.println("[Sending a request]");
    
    while (true) {
      VL53L0X_RangingMeasurementData_t measure;
      Serial.print("Reading a measurement... ");
      lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
      distance_mm = measure.RangeMilliMeter;

      Serial.println(distance_mm);
      client.print("TOF");
      client.print(distance_mm);
      delay(2000);
    }
  }
  else
  {
    Serial.println("connection failed!]");
    client.stop();
  }
  client.print(distance_mm);
  Serial.println("********************************");

}
