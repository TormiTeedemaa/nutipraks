#include <ESP8266WiFi.h>

const int pin_Pir = D4;           // passive infrared sensor pin

char ssid[] = "TormiWifi";           // SSID of your AP
char pass[] = "whyyouhackme";         // password of your AP

IPAddress server(192, 168, 4, 15); // IP address of the AP
WiFiClient client;

void setup() {
  Serial.begin(115200);
  pinMode(pin_Pir, INPUT);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

void loop() {
  client.connect(server, 80);
  Serial.println("********************************");

  Serial.println(digitalRead(pin_Pir));
  delay(2000);
  if (digitalRead(pir_pin) == HIGH ) {

    Serial.println("Pir detekteeris liikumist");

    client.println("PIR");
    String answer = client.readStringUntil('\r');
    client.flush();
    delay(1000);
  }

  if (digitalRead(pir_pin) == LOW ) {
    Serial.println("No movement any more");

    client.println("pir no motion");
    String answer = client.readStringUntil('\r');
    //Serial.println("From the AP: " + answer);
    client.flush();
    delay(1000);

  }
}
