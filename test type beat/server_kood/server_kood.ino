#include <ESP8266WiFi.h>
const char* ssid = "TormiWifi";
const char* password = "whyareyouhere";
int clientnumber = 0;
IPAddress IP(192, 168, 4, 15);
IPAddress mask = (255, 255, 255, 0);

#define MAX_CLIENTS 4
#define MAX_LINE_LENGTH 50
String request = "";
// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(8888);
WiFiClient *clients[MAX_CLIENTS] = { NULL };
char inputs[MAX_CLIENTS][MAX_LINE_LENGTH] = { 0 };

void setup() {
  Serial.begin(115200);
  delay(10);
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(IP, IP, mask);
  // Start the server
  server.begin();
  Serial.println("Server started");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());

}

void loop() {
  // Check if a new client has connected
  WiFiClient newClient = server.available();
  if (newClient) {
    Serial.println("new client");
    clientnumber += 1;
    // Find the first unused space
    for (int i = 0 ; i < clientnumber ; ++i) {
      if (NULL == clients[i]) {
        clients[i] = new WiFiClient(newClient);
        Serial.println(clients[i]->remoteIP());
        break;
      }
    }
  }

  // Check whether each client has some data
  for (int i = 0 ; i < clientnumber ; ++i) {
    // If the client is in use, and has some data...
    if (NULL != clients[i] && clients[i]->available()) {
      request = "";
      while (clients[i]->available()) {
        char ch = clients[i]->read();
        request += ch;
        delay(2);
      }
      Serial.println(request);
    }
    // Read the data
    //      char newChar = clients[i]->read();
    //      Serial.print(newChar);
    //      // If we have the end of a string
    //      // (Using the test your code uses)
    //      if ('\r' == newChar) {
    //
    //        // Blah blah, do whatever you want with inputs[i]
    //
    //        // Empty the string for next time
    //        inputs[i][0] = NULL;
    //
    //        // The flush that you had in your code - I'm not sure
    //        // why you want this, but here it is
    //        clients[i]->flush();

    // If you want to disconnect the client here, then do this:
    clients[i]->stop();
    delete clients[i];
    clients[i] = NULL;
    if (Serial.available()) {
      String muutuja = Serial.readString();
      clients[i]->println(muutuja);
    }

    //      } else {
    //        // Add it to the string
    //        //strcat(inputs[i], newChar);
    //        // IMPORTANT: Nothing stops this from overrunning the string and
    //        //            trashing your memory. You SHOULD guard against this.
    //        //            But I'm not going to do all your work for you :-)
    //      }
  }
}
