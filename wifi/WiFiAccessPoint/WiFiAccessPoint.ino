
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#ifndef APSSID
#define APSSID "TormiWifi"
#define APPSK  "TERE"
#endif

/* Set these to your desired credentials. */
const char *ssid = "TormiWifi";
const char *password = "TERE";

WiFiServer server(80);

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/
void handleRoot() {
  //server.send(200, "text/html", "<h1>You are connected</h1>");
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  WiFiClient client = server.available();
  
  while(client.connected()){
    
    while (client.available()){
      int vastu = client.read();
      Serial.println(vastu);
      if (vastu == 1){
        client.println(0);
      }
      else{
        client.println(1);
      }
      delay(1000);
    }
  }
}
