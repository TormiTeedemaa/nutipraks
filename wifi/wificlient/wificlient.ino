
#include <ESP8266WiFi.h>



char ssid[] = "Wemos_AP";           // SSID of your AP
char pass[] = "Wemos_comm";         // password of your AP
int laserPin = D3;
IPAddress server(192, 168, 4,15);  // IP address of the AP
WiFiClient client;

void setup() {
  Serial.begin(115200);
  pinMode(laserPin, OUTPUT);
  digitalWrite(laserPin, HIGH);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

void loop() {
  client.connect(server, 80);
  Serial.println("********************************");
  Serial.print("Byte sent to the AP: ");

  int value = analogRead(A0);
  Serial.print("Analog Value :");
  Serial.println(value);
  delay(1000);

  if (value < 140) {
    client.println("light");
    digitalWrite(laserPin, LOW);
    Serial.println(digitalRead(laserPin));
    String answer = client.readStringUntil('\r');
    Serial.println("From the AP: " + answer);
    client.flush();
    delay(100);
  }
  else if (value > 140) {
    client.println("no light");

  }

  delay(250);

  client.stop();
  delay(2000);
}
